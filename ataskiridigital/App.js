import React from 'react';
import { Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import Navigation from './src/Screen/Navigation';

const App = () => (
  <Provider theme={theme}>
    <Navigation />
  </Provider>
);

export default App;
