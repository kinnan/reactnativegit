import React, { memo } from 'react';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Paragraph from '../components/Paragraph';
import Button from '../components/Button';

const HomeScreen = ({ navigation }) => (
  <Background>
    <Logo />
    <Header>Atas Kiri Digital</Header>
    <Paragraph>
      We are utilizing and uplefting.
    </Paragraph>
    <Button mode="contained" onPress={() => navigation.navigate('Login')}>
      Login
    </Button>
    <Button
      mode="outlined"
      onPress={() => navigation.navigate('Register')}>
      Sign Up
    </Button>
  </Background>
);

export default memo(HomeScreen);
