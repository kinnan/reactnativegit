import React from 'react';
import { createDrawerNavigator} from '@react-navigation/drawer';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Dashboard from './Dashboard';
import HomeScreen from './HomeScreen';
import { LoginScreen, RegisterScreen } from '../components/auth';
import MainScreen from './MainScreen/MainScreen';
import SecondScreen from './SecondScreen/SecondScreen';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createMaterialBottomTabNavigator();

function DetailScreen() {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Dashboard" component={MyTab} />
            <Drawer.Screen name="Logout" component={HomeScreen} />
        </Drawer.Navigator>
    )
}

function MyTab() {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Dashboard" component={MainScreen} />
            <Tab.Screen name="Logout" component={HomeScreen} />
            
        </Tab.Navigator>
    )
}


function Navigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="Dashboard" component={DetailScreen} />
                <Stack.Screen name="Login" component={LoginScreen} />
                <Stack.Screen name="Register" component={RegisterScreen} />
                <Stack.Screen name="SecondScreen" component={SecondScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default Navigator;
