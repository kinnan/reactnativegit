import React, { memo, useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import Background from '../Background';
import Logo from '../Logo';
import Header from '../Header';
import Button from '../Button';
import TextInput from '../TextInput';
import BackButton from '../BackButton';
import { theme } from '../../core/theme';
import { emailValidator, passwordValidator } from '../auth/utils';
import Axios from 'axios';

const LoginScreen = ({ navigation }) => {
  const [username, setUsername] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });

  const _onLoginPressed = () => {
    // const usernameError = usernameValidator(username.value);
    // const passwordError = passwordValidator(password.value);
    successfulLogin()
    // if (emailError || passwordError) {
    //   setUsername({ ...username, error: usernameError });
    //   setPassword({ ...password, error: passwordError });
    //   return;
    // }
  };

  const successfulLogin = async () => {
    Axios
      .post('https://latihan-user-article.herokuapp.com/api/auth/signin', {
        username: username.value,
        password: password.value,
    })
      .then(function (res) {
        console.log(res.data);
        storeToken(res.data.accessToken, res.data.roles[0]);
        navigation.navigate('Dashboard');
      })
      .catch(function (error) {
        console.log(JSON.stringify(error.response.data, null, 2));
        alert(error);
      });

    const storeToken = async (token, role) => {
      try {
        await AsyncStorage.setItem(
          'token',
          token
        );
        // await AsyncStorage.setItem(
        //   'role',
        //   role
        // );
        console.log('token login' + await AsyncStorage.getItem('token'));
        // console.log('role login' + await AsyncStorage.getItem('role'));
      } catch (error) {
        console.log(error)
      }
    };
  }

  return (
    <Background>
      <BackButton goBack={() => navigation.navigate('Home')} />
      <Logo />
      <Header>Atas Kiri Digital</Header>
      <TextInput
        label="Username"
        returnKeyType="next"
        value={username.value}
        onChangeText={text => setUsername({ value: text, error: '' })}
        error={!!username.error}
        errorText={username.error}
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        autoCapitalize="none"
        value={password.value}
        onChangeText={text => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <Button mode="contained" onPress={_onLoginPressed}>
        Login
      </Button>
      <View style={styles.row}>
        <Text style={styles.label}>Don’t have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text style={styles.link}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
});

export default memo(LoginScreen);
